#!/bin/bash

trap "pkill myProg" EXIT

for i in `seq 1 $2`;
do
    #gdb -ex=r --args ./myProg -i $i -n $2 -o $4 &
    ./myProg -i $i -n $2 -o $4 &
#    cp myProg myProgxyz$i
#    ./myProgxyz$i -i $i -n $2 -o $4 &
done

read
