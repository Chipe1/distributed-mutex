#include<map>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<pthread.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<rpc/rpc.h>
#include<rpc/pmap_clnt.h>
#include<rpc/xdr.h>
#include<time.h>
#include "talk.pb.h"

using namespace std;

#define PROGNUM 142857
#define LOCKREQUEST 1
#define UNLOCKREQUEST 2
#define SENDACKREQUEST 3

static struct timeval TIMEOUT = {25, 0};

enum{
    DBG_FILECHANGE = 1<<0,
    DBG_IDENTITY = 1<<1,
    DBG_FLOW = 1<<2,
    DBG_MSG = 1<<3,
    DBG_LOCK = 1<<4
};
#define deBug (DBG_FILECHANGE | DBG_IDENTITY | DBG_FLOW | DBG_MSG | DBG_LOCK)&0

typedef struct __pb_wrapper{
    int chk;
    unsigned int slen;
    char *s;
}PBW;

typedef struct __st_msg{
    PBW data;
    struct __st_msg *next;
}MSG;

void dispatch(svc_req *, SVCXPRT *);
void *svc_run_wrapper(void *);
int lock(void);
void *broadcast_lock_req(void *);
void LockRequest(PBW *, svc_req *);
int unlock(void);
void UnlockRequest(PBW *, svc_req *);
void* send_ack(void *);
void SendAckRequest(PBW *, svc_req *);
bool_t xdr_proto(XDR *, PBW *);
void enqueue(PBW);
void destroy_queue(void);
int comp_clk(map<int, int>, map<int, int>);
map<int, int> merge_clk(map<int, int>, map<int, int>);
void print_clock(int, map<int, int>);
talk::NodeMessage get_protoclock(int, map<int, int>);
map<int, int> get_genclock(talk::NodeMessage);
PBW protowrap(talk::NodeMessage);
talk::NodeMessage protounwrap(PBW);
