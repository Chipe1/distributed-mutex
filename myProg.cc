#include "myProg.h"

int myID, n, fout, mainfile, me_interested, lock_wait_cnt, *already_ack, *asked_lock, got_all_clnt;
pthread_t server_tid, broadcast_tid;
pthread_mutex_t client_server_lock, client_broadcast_lock, *clnt_lock_ar, common_clnt_lock;
pthread_cond_t client_server_cond;
SVCXPRT *my_xprt;
CLIENT **clnts;
MSG *wait_queue;
map <int, int> genC, lockClock;

int main(int argc, char *argv[]){
    if(argc != 7 || strncmp(argv[1], "-i", 2) || strncmp(argv[3], "-n", 2) || strncmp(argv[5], "-o", 2)){
	dprintf(2, "Usage: myProg ­i <myId> ­n <N> ­o <outputFile>\n");
	exit(1);
    }
    srand(time(NULL));

    char foutname[102];
    sprintf(foutname, "out.%d.myProg", atoi(argv[2]));

    // output file
    //    mainfile = open(argv[6], O_CREAT|O_WRONLY|O_TRUNC, 0644);
    fout = open(foutname, O_CREAT|O_WRONLY|O_TRUNC, 0644);
    if(deBug & DBG_FILECHANGE)
	fout = 1;
    if(mainfile < 0){
	perror("[FATAL]Output file");
	exit(1);
    }
    if(fout < 0){
	perror("[FATAL]Log file");
	exit(1);
    }

    GOOGLE_PROTOBUF_VERIFY_VERSION;
    
    myID = atoi(argv[2]);
    n = atoi(argv[4]);
    if(deBug & DBG_IDENTITY){
	dprintf(fout, "ID: %d\tn: %d\n\n", myID, n);
	fsync(fout);
    }
    got_all_clnt = 0;

    // use mutex and conditional variables to coordinate read/write of common variables
    if(pthread_mutex_init(&client_server_lock, NULL) != 0 || pthread_mutex_init(&client_broadcast_lock, NULL) != 0){
	perror("[FATAL]mutex creation");
	exit(1);
    }
    if(pthread_mutex_init(&common_clnt_lock, NULL) != 0){
	perror("[FATAL]mutex creation");
	exit(1);
    }
    if(pthread_cond_init(&client_server_cond, NULL) != 0){
	perror("[FATAL]cond creation");
	exit(1);
    }
    me_interested = 0;
    lock_wait_cnt = 0;
    wait_queue = NULL;

    int i, j;
    clnts = (CLIENT **)malloc(sizeof(CLIENT *) * n);
    already_ack = (int *)malloc(sizeof(int) * n);
    asked_lock = (int *)malloc(sizeof(int) * n);
    clnt_lock_ar = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * n);
    for(i = 0;i < n; i++){
	clnts[i] = NULL;
	if(pthread_mutex_init(clnt_lock_ar + i, NULL) != 0){
	    perror("[FATAL]mutex creation");
	    exit(1);
	}
    }
    genC[myID - 1] = 1;
    // start server
    if(pthread_create(&server_tid, NULL, svc_run_wrapper, NULL)){
	perror("[FATAL]svc_run thread");
	exit(1);
    }

    
    usleep(100000);

    // client array
    j = 0;
    while(j < n-1){
	for(i = 0;i < n; i++){
	    if(i != myID - 1 && clnts[i] == NULL){
		clnts[i] = clnt_create("localhost", PROGNUM, i + 1, "tcp");
		if(clnts[i] == (CLIENT *)NULL){
		}
		else{
		    if(deBug & DBG_MSG){
			dprintf(fout, "[FOUND]%d: cliented %d\n", myID, i + 1);
		    }
		    j += 1;
		}
	    }
	}
	usleep(1000);
    }
    if(deBug & DBG_FLOW){
	dprintf(fout, "%d: Got all clients\n", myID);
    }
    got_all_clnt = 1;


    // client code
    for(i = 0; i < 101; i++){
	int sl_time;
	
	lock();
	
	if(deBug & DBG_FLOW){
	    dprintf(fout, "%d entered critical section\n", myID);
	}
	
	sl_time = 1 + ((rand())%(2*n - 1));
	mainfile = open(argv[6], O_CREAT|O_RDWR|O_APPEND, 0644);
	if(mainfile < 0){
	    char s[102];
	    sprintf(s, "[FATAL]%d couldnt open main file %s\n", myID, argv[6]);
	    perror(s);
	    exit(1);
	}
	
	j = lseek(mainfile, -2, SEEK_END);
	if(j<0){
	    // file was empty
	    dprintf(mainfile, "%d : %d : ", 1, myID);print_clock(mainfile, lockClock);dprintf(mainfile, "\n");
	    if(deBug & DBG_FLOW){
		dprintf(fout, "[WRITE]%d counter:%d\n", myID, 1);
	    }
	}
	else{
	    int counter;
	    char s[102];
	    while(j > 0){
		lseek(mainfile, j, SEEK_SET);
		read(mainfile, s, 1);
		if(*s == '\n')
		    break;
		j--;
	    }
	    if(j == 0){
		lseek(mainfile, j, SEEK_SET);
	    }
	    read(mainfile, s, 100);
	    sscanf(s, "%d", &counter);
	    lseek(mainfile, 0, SEEK_END);
	    if(counter < 100){
		dprintf(mainfile, "%d : %d : ", counter + 1, myID);print_clock(mainfile, lockClock);dprintf(mainfile, "\n");
		if(deBug & DBG_FLOW){
		    dprintf(fout, "[WRITE]%d counter:%d\n", myID, counter + 1);
		}
	    }
	    else{
		i = 101;
		if(deBug & DBG_FLOW){
		    dprintf(fout, "[NOWRITE]%d counter:%d, no use writing\n", myID, counter + 1);
		}
	    }
	}
	if(deBug & DBG_FLOW){
	    dprintf(fout, "%d will sleep for %d\n", myID, sl_time);
	    fsync(fout);
	}
	close(mainfile);
	
	unlock();
	
	if(deBug & DBG_FLOW){
	    dprintf(fout, "[SLEEP]%d\n", myID);
	}
	
	usleep(sl_time * 100000);
	
	if(deBug & DBG_FLOW){
	    dprintf(fout, "[WAKEY!]%d\n", myID);
	}
    }
    dprintf(fout, "[DONE]%d\n", myID);
    while(142857){
	pause();
    }
    dprintf(2, "[FATAL]%d is quiting\n", myID);
    return 0;
}

void dispatch(svc_req *req, SVCXPRT *xprt){
    xdrproc_t xdr2arg, arg2xdr;
    void (*local)(PBW *, svc_req *);
    PBW arg;
    void *result;

    if(deBug & DBG_MSG){
	dprintf(fout, "%d: got some mesage\n", myID);
    }
    
    switch(req->rq_proc){
    case NULLPROC:
	if(deBug & DBG_MSG){
	    dprintf(fout, "[NULL]%d got NULLPROC\n", myID);
	}
	if(svc_sendreply(xprt, (xdrproc_t)xdr_void, NULL)){
	    perror("NULL reply");
	}
	return ;
    case LOCKREQUEST:
	xdr2arg = (xdrproc_t)xdr_proto;
	arg2xdr = (xdrproc_t)xdr_void;
	local = LockRequest;
	break;
    case UNLOCKREQUEST:
	xdr2arg = (xdrproc_t)xdr_proto;
	arg2xdr = (xdrproc_t)xdr_void;
	local = UnlockRequest;
	break;
    case SENDACKREQUEST:
	xdr2arg = (xdrproc_t)xdr_proto;
	arg2xdr = (xdrproc_t)xdr_void;
	local = SendAckRequest;
	break;
    default:
	if(deBug & DBG_MSG){
	    dprintf(fout, "[!!!]%d got Unknown message\n", myID);
	}
	svcerr_noproc(xprt);
	return ;
    }

    // Null pointer recieves data from xdr
    memset((char *)&arg, 0, sizeof(arg));
    if(!svc_getargs(xprt, xdr2arg, (caddr_t)&arg)){
	perror("svc_getargs");
	svcerr_decode(xprt);
	return ;
    }
    result = NULL;
    if(!svc_sendreply(xprt, arg2xdr, (caddr_t)&result)) {
	perror("svc_sendreply");
	svcerr_systemerr(xprt);
    }
    if(deBug & DBG_MSG){
	    dprintf(fout, "%d replied to message\n", myID);
    }
    local(&arg, req);

    return;
}

void *svc_run_wrapper(void *unused){
    // unbind previous instances
    if(!pmap_unset(PROGNUM, myID)){
	perror("pmap_unset");
    }
    
    // create tcp RPC transport
    my_xprt = svctcp_create(RPC_ANYSOCK, 0, 0);
    if(deBug & DBG_MSG){
	dprintf(fout, "%d:SOCKnum: %d\tPORTnum: %d\n", myID, my_xprt->xp_sock, my_xprt->xp_port);
	fsync(fout);
    }
	    
    // register with nameserver
    if(!svc_register(my_xprt, PROGNUM, myID, dispatch, IPPROTO_TCP)){
	perror("[FATAL]svc_register");
	exit(1);
    }
    if(deBug & DBG_MSG){
	dprintf(fout, "Registered version %d on %d\n", myID, PROGNUM);
	fsync(fout);
    }
    svc_run();

    // should never be printed
    dprintf(2, "[FATAL]svc_run returned\n");
    exit(1);
}

int lock(void){
    int i;
    // initialize/assign variables
    pthread_mutex_lock(&client_server_lock);
    
    if(deBug & DBG_LOCK){
	dprintf(fout, "[LOCK]%d lock 1\n", myID);
    }
    
    me_interested = 1;
    lockClock = merge_clk(genC, genC);
    for(i = 0;i < n; i++){
	already_ack[i] = 0;
	asked_lock[i] = 0;
    }
    genC[myID - 1]++;
    lock_wait_cnt = n - 1;
    
    if(deBug & DBG_LOCK){
	dprintf(fout, "[UNLOCK]%d lock 1\n", myID);
    }
    
    pthread_mutex_unlock(&client_server_lock);

    // new thread send requests only after the current thread waits on cond
    pthread_mutex_lock(&client_broadcast_lock);
    
    if(pthread_create(&broadcast_tid, NULL, broadcast_lock_req, NULL)){
	perror("[FATAL]svc_run thread");
	exit(1);
    }
    
    pthread_detach(broadcast_tid);

    if(deBug & DBG_FLOW){
	dprintf(fout, "%d: will go on cond\n", myID);
    }

    // wait for signal from server
    pthread_cond_wait(&client_server_cond, &client_broadcast_lock);

    if(deBug & DBG_FLOW){
	dprintf(fout, "%d: got  cond\n", myID);
    }

    pthread_mutex_unlock(&client_broadcast_lock);

    // LOCK ACQUIRED
    dprintf(fout, "%d acquired lock time: ", myID);print_clock(fout, lockClock);dprintf(fout, "\n");
    fsync(fout);
    
    return 0;
}

void *broadcast_lock_req(void *unused){
    int i;
    talk::NodeMessage msg;
    string s;
    char *resp;

    // protobuf byte array
    msg = get_protoclock(myID, lockClock);

    // Null array recieves from xdr
    resp = NULL;

    // wait for client to wait on cond
    pthread_mutex_lock(&client_broadcast_lock);
    
    if(deBug & DBG_FLOW){
	dprintf(fout, "%d: client waits on cond\n", myID);
    }
    
    // no actual use of lock. unlock it
    pthread_mutex_unlock(&client_broadcast_lock);

    // Send message to peers
    for(i=0;i<n;i++){
	if(i != myID - 1){
	    int call_ret;
	    dprintf(fout, "%d-->LockRequest(%d)\treq_clock: ", myID, i+1);print_clock(fout, lockClock);dprintf(fout, "\tclock: ");print_clock(fout, genC);dprintf(fout, "\n");
	    fsync(fout);
	    PBW *topush = (PBW *)malloc(sizeof(PBW));
	    *topush = protowrap(msg);
	    //	    pthread_mutex_lock(clnt_lock_ar + i);
	    pthread_mutex_lock(&common_clnt_lock);
	    while((call_ret=clnt_call(clnts[i], LOCKREQUEST, (xdrproc_t)xdr_proto, (caddr_t)topush, (xdrproc_t)xdr_void, (caddr_t)&resp, TIMEOUT)) != RPC_SUCCESS){
		if(deBug & DBG_MSG){
		    dprintf(fout, "[RESEND %d]%d-->LockRequest(%d)\n", call_ret, myID, i+1);
		}
		if(call_ret != 9)
		    break;
		pthread_mutex_unlock(&common_clnt_lock);
		usleep(10000);
		pthread_mutex_lock(&common_clnt_lock);
	    }
	    pthread_mutex_unlock(&common_clnt_lock);
	    //	    pthread_mutex_unlock(clnt_lock_ar + i);
	}
    }
    return NULL;
}

void LockRequest(PBW *arg, svc_req *req){
    talk::NodeMessage msg;
    map<int, int> reqClock;
    int do_send_ack;

    if(((arg->slen) ^ (arg->chk)) != 149){
	dprintf(fout, "%d: Erroneous(PBW) message %d %d\n", myID, arg->slen, arg->chk);
	return ;
    }
    
    msg = protounwrap(*arg);
    
    if(((msg.id()) ^ (msg.chk())) != 149){
	dprintf(fout, "%d: Erroneous(PROTO) message %d %d\n", myID, msg.id(), msg.chk());
	return ;
    }

    reqClock = get_genclock(msg);
    
    pthread_mutex_lock(&client_server_lock);
    if(deBug & DBG_LOCK){
	dprintf(fout, "[LOCK]%d LockRequest\n", myID);
    }
    dprintf(fout, "%d<--LockRequest(%d)\treq_clock: ", myID, msg.id());print_clock(fout, reqClock);dprintf(fout, "\tclock: ");print_clock(fout, genC);dprintf(fout, "\n");

    genC = merge_clk(genC, reqClock);
    do_send_ack = 1;
    if(me_interested){
	// clock check
	if(lock_wait_cnt == 0 || comp_clk(lockClock, reqClock) < 0){
	    // we won the tie
	    if(deBug & DBG_FLOW){
		dprintf(fout, "%d(%d) wont give lock to %d ", myID, lock_wait_cnt, msg.id());print_clock(fout, lockClock);dprintf(fout, "\n");
		fsync(fout);
	    }
	    if(asked_lock[msg.id() - 1]){
		dprintf(fout, "[BAD]%d: relock request from %d\n", myID, msg.id());
	    }
	    else{
		enqueue(*arg);
		asked_lock[msg.id() - 1] = 1;
	    }
	    do_send_ack = 0;
	}
	else{
	    if(deBug & DBG_FLOW){
		dprintf(fout, "%d(%d) compromises lock %d", myID, lock_wait_cnt, msg.id());print_clock(fout, lockClock);dprintf(fout, "\n");
		fsync(fout);
	    }
	}
    }
    else{
	if(deBug & DBG_FLOW){
	    dprintf(fout, "%d doesnt need lock %d\n", myID, msg.id());
	    fsync(fout);
	}
    }
    if(do_send_ack){
	pthread_t acksender;
	PBW *targ = (PBW *)malloc(sizeof(PBW));
	*targ = protowrap(msg);

	if(deBug & DBG_MSG){
	    dprintf(fout, "%d: Creating ack sender\n", myID);
	}

	if(pthread_create(&acksender, NULL, send_ack, (void *)targ)){
	    perror("[FATAL]send_ack thread");
	    exit(1);
	}

	pthread_detach(acksender);
	if(deBug & DBG_MSG){
	    dprintf(fout, "%d: detached ack sender\n", myID);
	}
    }
    
    if(deBug & DBG_LOCK){
	dprintf(fout, "[UNLOCK]%d LockRequest\n", myID);
    }
    
    pthread_mutex_unlock(&client_server_lock);
}

int unlock(void){
    int i;
    talk::NodeMessage msg;
    char *resp;
    MSG *temp;

    pthread_mutex_lock(&client_server_lock);
    
    if(deBug & DBG_LOCK){
	dprintf(fout, "[LOCK]%d unlock 1\n", myID);
    }
    
    dprintf(fout, "%d unlocked: ", myID);print_clock(fout, lockClock);dprintf(fout, "\n");
    
    me_interested = 0;
    lock_wait_cnt = 0;
    
    if(deBug & DBG_LOCK){
	dprintf(fout, "[UNLOCK]%d unlock 1\n", myID);
    }
    
    pthread_mutex_unlock(&client_server_lock);

    // protobuf byte array
    msg = get_protoclock(myID, genC);

    // Null array recieves from xdr
    resp = NULL;

    // Send message to peers
    temp = wait_queue;
    while(temp){
	int call_ret;
	talk::NodeMessage reqMsg;
	reqMsg = protounwrap(temp->data);
	i = reqMsg.id();
	dprintf(fout, "%d-->UnlockRequest(%d)\treq_clock: ", myID, i);print_clock(fout, get_genclock(reqMsg));dprintf(fout, "\tclock: ");print_clock(fout, genC);dprintf(fout, "\n");

	PBW *topush = (PBW *)malloc(sizeof(PBW));
	*topush = protowrap(msg);
	pthread_mutex_lock(&common_clnt_lock);
	if((call_ret = clnt_call(clnts[i - 1], UNLOCKREQUEST, (xdrproc_t)xdr_proto, (caddr_t)topush, (xdrproc_t)xdr_void, (caddr_t)&resp, TIMEOUT)) != RPC_SUCCESS){
	    if(deBug & DBG_MSG){
		dprintf(fout, "[RESEND %d]%d-->UnlockRequest(%d)\n", call_ret, myID, i);
	    }
	}
	pthread_mutex_unlock(&common_clnt_lock);
	temp = temp->next;
    }

    pthread_mutex_lock(&client_server_lock);

    if(deBug & DBG_LOCK){
	dprintf(fout, "[LOCK]%d unlock 2\n", myID);
    }

    destroy_queue();

    if(deBug & DBG_LOCK){
	dprintf(fout, "[UNLOCK]%d unlock 2\n", myID);
    }

    pthread_mutex_unlock(&client_server_lock);
    return 0;
}

void UnlockRequest(PBW *arg, svc_req *req){
    dprintf(fout, "%d<--PIGGY\n", myID);
    SendAckRequest(arg, req);
}

void* send_ack(void * arg){
    int i, call_ret;
    talk::NodeMessage msg;
    char *resp;

    // protobuf byte array
    msg = protounwrap(*(PBW *)arg);
    free(arg);
    i = msg.id();
    
    // Null array recieves from xdr
    resp = NULL;

    // send ack to peer
    while(got_all_clnt != 1){
	dprintf(fout, "%d will sleep for all clnts 1\n", myID);
	usleep(10000);
	dprintf(fout, "%d will sleep for all clnts 2\n", myID);
    }

    pthread_mutex_lock(&client_server_lock);
    if(deBug & DBG_LOCK){
	dprintf(fout, "[LOCK]%d LockRequest\n", myID);
    }
    dprintf(fout, "%d-->Ack(%d)\treq_clock: ", myID, msg.id());print_clock(fout, get_genclock(msg));dprintf(fout, "\tclock: ");print_clock(fout, genC);dprintf(fout, "\n");
    msg = get_protoclock(myID, genC);
    if(deBug & DBG_LOCK){
	dprintf(fout, "[UNLOCK]%d LockRequest\n", myID);
    }
    
    pthread_mutex_unlock(&client_server_lock);

    PBW *topush = (PBW *)malloc(sizeof(PBW));
    *topush = protowrap(msg);

    pthread_mutex_lock(&common_clnt_lock);
    if((call_ret = clnt_call(clnts[i - 1], SENDACKREQUEST, (xdrproc_t)xdr_proto, (caddr_t)topush, (xdrproc_t)xdr_void, (caddr_t)&resp, TIMEOUT)) != RPC_SUCCESS){
	if(deBug & DBG_MSG){
	    dprintf(fout, "[RESEND %d]%d-->ACK(%d)\n", call_ret, myID, i);
	}
    }

    pthread_mutex_unlock(&common_clnt_lock);
    return NULL;
}

void SendAckRequest(PBW *arg, svc_req *req){
    talk::NodeMessage msg;
    map<int, int> reqClock;
    
    if((arg->slen ^ arg->chk) != 149){
	dprintf(fout, "%d: Erroneous(PBW) message %d %d\n", myID, arg->slen, arg->chk);
	return ;
    }

    msg = protounwrap(*arg);
    
    if(((msg.id()) ^ (msg.chk())) != 149){
	dprintf(fout, "%d: Erroneous(PROTO) message %d %d\n", myID, msg.id(), msg.chk());
	return ;
    }

    reqClock = get_genclock(msg);
    
    pthread_mutex_lock(&client_server_lock);
    
    dprintf(fout, "%d<--Ack(%d)\treq_clock: ", myID, msg.id());print_clock(fout, reqClock);dprintf(fout, "\tclock: ");print_clock(fout, genC);dprintf(fout, "\n");
    if(deBug & DBG_LOCK){
	dprintf(fout, "[LOCK]%d SendAck\n", myID);
    }
    
    if(me_interested == 0 || lock_wait_cnt <= 0){
	dprintf(2, "[ERROR]%d<--Funny message(%d)\n", myID, msg.id());
    }
    if(already_ack[msg.id() - 1] == 1){
	dprintf(fout, "[IGNORE]%d!!!already ack %d timestamp ", myID, msg.id());print_clock(fout, reqClock);dprintf(fout, "\n");
    }
    else{
	already_ack[msg.id() - 1] = 1;
	lock_wait_cnt--;

	if(deBug & DBG_FLOW){
	    dprintf(fout, "%d lowers count to %d due to %d\n", myID, lock_wait_cnt, msg.id());
	    fsync(fout);
	}
	if(lock_wait_cnt == 0){
	    pthread_cond_signal(&client_server_cond);
	    if(deBug & DBG_FLOW){
		dprintf(fout, "%d: granting lock\n", myID);
	    }
	}
    }
    if(deBug & DBG_LOCK){
	dprintf(fout, "[UNLOCK]%d SendAck\n", myID);
    }
    pthread_mutex_unlock(&client_server_lock);
}

bool_t xdr_proto(XDR *xdrs, PBW *arg){
    if(!xdr_int(xdrs, &(arg->chk))){
	perror("xdr_proto");
	return FALSE;
    }
    if(!xdr_bytes(xdrs, &(arg->s), &(arg->slen), 10000)){
	perror("xdr_proto");
	return FALSE;
    }
    return TRUE;
}

void enqueue(PBW data){
    MSG *temp;
    temp = (MSG *)malloc(sizeof(MSG));
    temp->data = data;
    temp->next = wait_queue;
    wait_queue = temp;
}

void destroy_queue(void){
    MSG *temp;
    while(wait_queue != NULL){
	temp = wait_queue->next;
	free(wait_queue);
	wait_queue = temp;
    }
}

int comp_clk(map<int, int> a, map<int, int> b){
    // returns -1 if a < b
    map<int, int>::iterator it1, it2;
    for(it1 = a.begin(), it2 = b.begin();it1 != a.end() && it2 != b.end(); it1++, it2++){
	int keya,keyb;
	int vala,valb;
	keya = it1->first;
	keyb = it2->first;
	vala = it1->second;
	valb = it2->second;
	if(keya<keyb){
	    return -1;
	}
	else if(keya>keyb){
	    return 1;
	}
	else if(vala<valb){
	    return -1;
	}
	else if(vala>valb){
	    return 1;
	}
    }
    if(it2 != b.end()){
	return -1;
    }
    else if(it1 != a.end()){
	return 1;
    }
    dprintf(2, "[COMPERROR]%d\n", myID);
    return 0;
}

map<int, int> merge_clk(map<int, int> a, map<int, int> b){
    map<int, int> toret;
    for(map<int, int>::iterator it = a.begin(); it!=a.end(); it++){
	int key, value;
	key = it->first; value = it->second;
	toret[key] = max(toret[key], value);
    }
    for(map<int, int>::iterator it = b.begin(); it!=b.end(); it++){
	int key, value;
	key = it->first; value = it->second;
	toret[key] = max(toret[key], value);
    }
    return toret;
}

void print_clock(int fd, map<int, int> m){
    dprintf(fd, "{");
    for(map<int, int>::iterator it = m.begin(); it!=m.end();){
	dprintf(fd, "%d: %d", it->first + 1, it->second);
	if(++it != m.end())
	    dprintf(fd, ", ");
    }
    dprintf(fd, "}");
}

talk::NodeMessage get_protoclock(int id, map<int, int> m){
    talk::NodeMessage toret;
    toret.set_id(id);
    toret.set_chk(id^149);
    for(map<int, int>::iterator it = m.begin(); it!=m.end(); it++){
	talk::NodeMessage_ClockElement *ele = toret.add_genclock();
	ele->set_id(it->first);
	ele->set_clock(it->second);
    }
    return toret;
}

map<int, int> get_genclock(talk::NodeMessage msg){
    map<int, int> toret;
    for(int i = 0; i < msg.genclock_size(); i++){
	talk::NodeMessage_ClockElement ele = msg.genclock(i);
	toret[ele.id()] = ele.clock();
    }
    return toret;
}

PBW protowrap(talk::NodeMessage msg){
    string str;
    PBW toret;
    
    msg.SerializeToString(&str);
    toret.slen = str.size();
    toret.chk = toret.slen ^ 149;
    toret.s = (char *)malloc(sizeof(char) * (toret.slen + 1));
    str.copy(toret.s, toret.slen, 0);
    toret.s[toret.slen] = '\0';
    return toret;
}

talk::NodeMessage protounwrap(PBW obj){
    talk::NodeMessage toret;
    string str;
    
    str.assign(obj.s, obj.slen);
    toret.ParseFromString(str);
    return toret;
}
