.PHONY:	all	clean	re	run
run: 	myProg
	./script.sh -n 37 -o out.f

all:	myProg

myProg:	myProg.cc myProg.h talk.pb.cc talk.pb.h
	c++ -pthread -g -Wall $< talk.pb.cc -o $@ `pkg-config --cflags --libs protobuf`

clean:
	rm myProg

re:	clean	all

